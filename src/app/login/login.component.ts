import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  text:string;
  code:string;
  message:string;
  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit() {
  }
  login(){
    console.log(this.email)
    this.auth.login(this.email, this.password).then(value => {
      this.router.navigate(['/items']);
    }).catch(value =>{
      console.log(value); 
      this.code=value.code;
      this.message=value.message;
    })

  }

}
