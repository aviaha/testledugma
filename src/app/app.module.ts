import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input'; 
import { Routes,RouterModule } from '@angular/router';
import{environment} from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { RegisterComponent } from './register/register.component';
import { ItemComponent } from './item/item.component';
import { ItemsComponent } from './items/items.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { GreetingsComponent } from './greetings/greetings.component';  
import {MatCheckboxModule} from '@angular/material/checkbox';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    RegisterComponent,
    ItemComponent,
    ItemsComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    MatButtonModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
      RouterModule.forRoot([//הגדרת הראוטים
        {path:'',component:NotFoundComponent},
        {path:'Item',component:ItemComponent},
        {path:'Items',component:ItemsComponent},
        {path:'NotFound',component:NotFoundComponent},
        {path:'Login',component:LoginComponent},
        {path:'**',component:NotFoundComponent}

  
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
