import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class ItemsService {

update(key,done){
  this.authService.user.subscribe(user=>{
    this.db.list('/users/'+user.uid).update(key,{'stock':done})
})
}
  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid).remove(key);
  })}
  
  constructor(private authService:AuthService,
              private db:AngularFireDatabase  ) { }
}
