import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;
   name:string;
    price:string;
    key:string;
    afterclick:boolean
    show2:boolean;
    showbutton1:boolean;
    strong:boolean;
    checked:boolean;
    
update(){
  this.items.update(this.key,this.checked)
}
  constructor(private items:ItemsService) { }
  showbutton(){
    if(this.strong==false){
      console.log(this.strong)
   this.showbutton1=true
    }
    } 
  
  notshowbutton(){
    if(this.strong==false){
    this.showbutton1=false
    }
  }
  ngOnInit() {
  this.strong=false;
    this.name=this.data.name;
    this.checked=this.data.stock;
    this.price=this.data.price;
    this.key=this.data.$key;
  }
  delete(){
    console.log(this.key)
    this.items.delete(this.key)
  }
 
  show2button(){
  this.strong = true
  this.showbutton1=false
  this.show2 = true
  }
  cancel(){
    this.show2 =false;
    this.showbutton1=true;

  }

}
